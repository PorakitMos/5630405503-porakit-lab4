<?php

function array_to_xml(array $arr, SimpleXMLElement $xml) {
    foreach ($arr as $k => $v) {
        is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    }
    return $xml;
}
$Param = $_GET["alt"];

$arr = array('student' => array(
        array('name' => 'ปรกฤษณ',
            'books' => array('Java', 'สมองเศรษฐี', 'english for beginner'),
            'education' => array(
                array('highschool' => 'Khon Kaen Wittayayon'),
                array('undergrad' => 'Khon Kaen University'))),
        array('name' => 'สมชาย',
            'books' => array('how to android', 'Harry pooter', 'Hunger games'),
            'education' => array(
                array('highschool' => 'Satit KKU'),
                array('undergrad' => 'Khon Kaen University'))))
);
    if ($Param == 'xml'){
    	$writer = new XMLWriter();

		$writer->openMemory();
		$writer->setIndent(true);
		$writer->setIndentString("");
		$writer->startDocument("1.0", "UTF-8");

		$writer->startElement('students');

		$writer->startElement("student");

		$writer->writeElement("name", "ปรกฤษณ");

		$writer->startElement("books");
		$writer->writeElement("book", "Java");
		$writer->writeElement("book", "สมองเศรษฐี");
		$writer->writeElement("book", "english for beginner");
		$writer->endElement();

		$writer->startElement("education");
		$writer->writeElement("highschool", "Khon Kaen Wittayayon");
		$writer->writeElement("undergrad", "Khon Kaen University");
		$writer->endElement();

		$writer->endElement();

		$writer->startElement("student");

		$writer->writeElement("name", "มณฑล");

		$writer->startElement("books");
		$writer->writeElement("book", "how to android");
		$writer->writeElement("book", "Harry pooter");
		$writer->writeElement("book", "Hunger games");
		$writer->endElement();

		$writer->startElement("education");
		$writer->writeElement("highschool", "Satit KKU");
		$writer->writeElement("undergrad", "Kasetsart University");
		$writer->endElement();

		$writer->endElement();

		$writer->endDocument();
		header('Content-type: text/xml');

		echo $writer->outputMemory();
	}
	elseif ($Param =='json'){
		$x = json_encode($arr);
 		echo  json_encode($arr);
 	}
?>
