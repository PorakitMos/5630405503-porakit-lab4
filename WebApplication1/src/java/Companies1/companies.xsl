<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Companies.xsl
    Created on : September 7, 2016, 1:25 PM
    Author     : Porakit
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Companies.xsl</title>
            </head>
            <body>
                <ul><xsl:for-each select="companies/company">
                        <li><xsl:value-of select="name"/></li>
                    </xsl:for-each>
                </ul>
               
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
