<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies2.xsl
    Created on : September 7, 2016, 1:41 PM
    Author     : Porakit
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>companies2.xsl</title>
            </head>
            <body>
                <h2>These are the leading companies:</h2>
                <ul><xsl:for-each select="companies/company">
                        <li><xsl:value-of select="name"/></li>
                        <ul type="circle">
                            <xsl:for-each select="research/labs/lab">
                                <li>
                                <xsl:value-of select="."></xsl:value-of>
                                </li>
                            </xsl:for-each>
                            
                        </ul>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
